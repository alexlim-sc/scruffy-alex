<?php

  require("scapi.php");

  $endpoint = "hello";
  $method = "GET";
  $version = "v0";
  $data = "";
  $maskuser = "";
  $host = "localhost";


  if(isset($_POST['method']) && isset($_POST["endpoint"])) {

    
    $endpoint = $_POST['endpoint'];
    $method = $_POST['method'];
    $version = $_POST['version'];

    $data = "";
	if(isset($_POST['data'])) {
		$data = $_POST['data'];
	}

	if(isset($_POST['host'])) {
		$host = $_POST['host'];
	}

  if(isset($_POST['maskuser'])) {
    $maskuser = $_POST['maskuser'];
  }


    $response = scapi($method, $endpoint, $data, $host, $maskuser, $version);
    $json_string = json_encode(json_decode($response), JSON_PRETTY_PRINT);
    
    
  //echo scapi("GET", "/scapi/api/v0/timesheet?startdate=2016-01-01&enddate=2016-01-10", "");

  //echo scapi("PUT", "/scapi/api/v0/timesheet", '{"date":"2016-01-08","rows":{"0":{"name":"1234","comment":"worked hard","time":"90"},"1":{"name":"1235","comment":"worked less hard","time":"60"},"5":{"name":"1234","comment":"worked again ","time":"240"}}}');
  }
 ?>

  <form id="timeform" action="test.php" method="post">
        
        <table>
        	<tr><td>
  				Host:</td><td> <input type="text" value="<?php echo $host; ?>" name="host" />  <br/>    
  			</td></tr>
        <!--	<tr><td>
          Local:</td><td>http://scapi.dev.solarcentury.co.uk
        </td></tr>-->
        <tr><td>
  				Staging:</td><td>https://scapi-stage.solarcentury.com    
  			</td></tr>
        	<tr><td>
  				Dev:</td><td>https://scapi-dev.solarcentury.com    
  			</td></tr>
        	

        <tr><td>
          Version:</td><td> <input type="text" value="<?php echo $version; ?>" name="version" />  <br/>    
        </td></tr>
        	<tr><td>
  				Method:</td><td> <input type="text" value="<?php echo $method; ?>" name="method" />  <br/>    
  			</td></tr>
        <tr><td>
          Mask User:</td><td> <input type="text" value="<?php echo $maskuser; ?>" name="maskuser" />  <br/>    
        </td></tr>

  			<tr><td>
  				Endpoint:</td><td> <input type="text" value="<?php echo $endpoint; ?>" name="endpoint" />     <br/> 
  			</td></tr>
  			<tr><td>
  				Data:</td><td> <textarea style="width:500px; height:200px;" type="text" id="data" name="data" ><?php echo $data; ?></textarea>      <br/>
  			</td></tr>
  			<tr><td></td><td>
    			<button class="btn btn-success" type="submit">Go</a>
    		</td></tr>
    	</table>
  </form>   

  <?php
  		

		echo "<pre>$json_string</pre>";
		echo "<br>";
		echo date('Y-m-d\TH:i:s');
		echo "<br>";
		echo "<br>Raw Response<br>";
		echo "<pre>$response</pre>";
  ?>