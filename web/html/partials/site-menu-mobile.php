<h4>Project menu</h4>
<ul class="nav nav-pills nav-stacked">
  <li><a href="project-site.php">Overview</a></li>
  <li><a href="project-site-ticketing.php">Tickets</a></li>
  <li><a href="project-site-facets.php">Log progress</a></li>
  <li><a href="files.php">Documents</a></li>
  <li><a href="project-site-milestones.php">Milestones</a></li>
  <li><a href="#">Progress tracking</a></li>
  <li><a href="project-site-pipeline.php">Pipeline</a></li>
  <li><a href="#">People</a></li>
</ul>