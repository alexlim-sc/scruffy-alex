<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Jimmy Page">
  <meta name="keywords" content="HTML, CSS, JS, JavaScript, framework, bootstrap, front-end, frontend, web development">
  <meta name="author" content="Jimmy Page">

  <title>Sceptre</title>

  <!-- Bootstrap core CSS -->
  <link href="../assets/css/chosen.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
  <link href="../assets/css/wireframe.css" rel="stylesheet">
  <link href="../assets/css/wireframe-mike.css" rel="stylesheet">
  <link href="../assets/css/style.css" rel="stylesheet">
  <link href="../assets/css/datatables.min.css" rel="stylesheet">
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Favicons -->
  <link rel="apple-touch-icon-precomposed" href="/apple-touch-icon-precomposed.png">
  <link rel="icon" href="/frontend/favicon.ico">
</head>

<body>
<p class="wireframe-disclaimer">---------- WIREFRAME ----------</p>
<header>
  <div class="navigations">
    <nav id="global-nav" class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle search-toggle collapsed" data-toggle="collapse" data-target="#search-collapse" aria-expanded="false">
            <span class="sr-only">Toggle search</span>
            <span class="glyphicon glyphicon-search"></span>
          </button>
          <button type="button" class="menu-btn navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-container" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-label">MENU</span>
            <span class="icon">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </span>
          </button>
          <a class="navbar-brand" href="index.php">
            <span class="sc-logo">solarcentury</span>
          </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-collapse-container">
          <ul class="nav navbar-nav ">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="clients.php">Clients</a></li>
            <li><a href="projects.php">Projects</a></li>
            <li><a href="helpdesk.php">Helpdesk</a></li>
            <li><a href="wiki.php">Company pages</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Jonathan Laventhol<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">My account</a></li>
                <li><a href="#">My projects</a></li>
                <li><a href="#">My tasks</a></li>
                <li><a href="#">My milestones</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>

    <div id="secondary-nav" class="clearfix">
      <div class="row">
        <div class="col-sm-7 col-sm-push-5" id="search-container">
          <form id="search-collapse" class="navbar-form pull-right collapse search-collapse" role="search">
            <div class="input-group">
              <input type="text" class="form-control pull-left" placeholder="Search for anything&hellip;">
             <span class="input-group-btn"> <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span><span class="sr-only">Submit</span></button></span>
            </div>
            <div class="live-results">
              <dl>
                <dt class="results-group"><span>Sites</span></dt>
                <dd><a href="project-site.php">Screwfix - Southbank</a></dd>
                <dd><a href="project-site.php">Screwfix - Hove</a></dd>
                <dd><a href="project-site.php">Screwfix - Nottingham</a></dd>
                <dd><a href="search-results-clients.php"><strong>&hellip;view all 7 matches in clients</strong></a></dd>
                <dt class="results-group"><span>Clients</span></dt>
                <dd><a href="project-site.php">Screwfix</a></dd>
                <dt class="search-all"><a href="search-results.php">Search <span class="search-term"></span> in this site</a></dt>
                <dt class="search-all"><a href="search-results.php">Search <span class="search-term"></span> across everything</a></dt>
              </dl>
            </div>
          </form>

        </div>
        <div class="col-sm-5 col-sm-pull-7">
          <div class="page-title pull-left">
            <h2><a href="#"><?php echo $pageTitle; ?></a></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
