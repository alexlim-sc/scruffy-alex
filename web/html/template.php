<?php

  function setAttributeByClassName($DOM, $classname, $attribute, $value) {
    $finder = new DomXPath($DOM);
    $nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");
    for ($i = 0; $i < $nodes->length; $i++) {
      $nodes->item($i)->setAttribute($attribute, $value);
    }
  }

  function setInnerHtmlByClassName($DOM, $classname, $value) {
    $finder = new DomXPath($DOM);
    $nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");
    for ($i = 0; $i < $nodes->length; $i++) {
      $nodes->item($i)->nodeValue  = $value ;
    }
  }

  function setSelectByClassName($DOM, $classname, $value) {
    $finder = new DomXPath($DOM);
    $nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");
    for ($i = 0; $i < $nodes->length; $i++) {
      $item = $nodes->item($i);
      if ($item->hasChildNodes()) {
        $childs = $item->childNodes;
        foreach($childs as $i) {
        	if($i->getAttribute("value") == $value) {
        		$i->setAttribute("selected", "true");
        	}
            
        }
      }
    }
  }

function showMessage($title, $message) {

    
  $html = file_get_contents('messagetemplate.html');

  $DOM = new DOMDocument;
  $DOM->loadHTML($html);
  setInnerHtmlByClassName($DOM, "title", $title);
  setInnerHtmlByClassName($DOM, "message", $message);
  echo $DOM->saveHTML();

}

 
