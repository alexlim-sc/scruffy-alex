<?php

  include("checklogin.php");
  require("scapi.php");
  require("template.php");

  if(isset($_GET["date"])) {
    $currentDate = $_GET["date"];
  }


  if(isset($_POST["workDate"])) {

    
    $currentDate = $_POST['workDate'];

    $data = json_encode($_POST);

    $response = json_decode(scapi("PUT", "timesheet", $data), true);

    if($response['status'] === "success") {
      header("Location: timesheets.php?saved=$currentDate");
    } else {
      $error = $response['message'];
    }
  }


  function showError() {
    if(isset($error)) {
      showMessage("Error", $error);
    }
  }

  function showTimeRows($date) {

    global $numberOfIntialRows;
    $startDate = $date;
    $endDate = $date;  

    $html = file_get_contents('timerowtemplate.html');
    $timesheetDays = json_decode(scapi("GET", "timesheet?startdate=$startDate&enddate=$endDate", ""), true);
    //There should only be one day
     if(isset($timesheetDays[0])) {
       $currentDay = $timesheetDays[0];
     }
     

     foreach($currentDay['workTimes'] as $time) {
      addTimeRow($time, $html);

     }

     echo "<input type=\"hidden\" value=\"$numberOfIntialRows\" id=\"rowCount\" />";
  }

  $numberOfIntialRows = 0;

  function addTimeRow($timeRowTime, $html) {
    
    global $numberOfIntialRows;



    $DOM = new DOMDocument;
    $DOM->loadHTML($html);
    setAttributeByClassName($DOM, "jobname", "value", $timeRowTime['jobId']);
    setAttributeByClassName($DOM, "jobname", "name", "rows[$numberOfIntialRows][name]");
    setAttributeByClassName($DOM, "jobcomment", "value", $timeRowTime['comment']);
    setAttributeByClassName($DOM, "jobcomment", "name", "rows[$numberOfIntialRows][comment]");
    setSelectByClassName($DOM, "jobtime", $timeRowTime['workTime']);
    setAttributeByClassName($DOM, "jobtime", "name", "rows[$numberOfIntialRows][workTime]");

    $numberOfIntialRows += 1;
    echo $DOM->saveHTML();
  }


  

  $pageTitle = "Sceptre";
  include 'partials/header.php';





?>

<ol class="breadcrumb">
  <li><strong>You are here: </strong></li>
  <li>Timesheets</li>
</ol>

</header>

<div class="container-fluid">
  <div class="row">
    
      <h1><?php echo $currentDate; ?></h1>

      <?php showError(); ?>    


      <form id="timeform" action="edittimesheet.php" method="post">
        <input type="hidden" name="workDate" value="<?php echo $currentDate; ?>" />
        <div class="row">
          <div id="timerows" class="form-group">
            
            <?php showTimeRows($currentDate); ?>
            
          </div>

          

        </div>
              <a id="addTimeRow" class="btn btn-success">Add Time row</a>
              <button class="btn btn-success" type="submit">Submit time sheet</a>
      </form>





  </div>
</div>


<?php include 'partials/footer.php' ?>
