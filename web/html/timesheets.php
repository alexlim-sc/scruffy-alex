<?php

  include("checklogin.php");
  require("scapi.php");
   require("template.php");



  function getDayOfWeek($date) {
    return (intval(date('w', strtotime($date))) + 6) % 7;
  }


  $pageTitle = "Sceptre";
  include 'partials/header.php';
  
  function showSaved() {
    if(isset($_GET["saved"])) {
      showMessage("Saved", $_GET["saved"]);
    }  
  }
  



  if(isset($_GET["startdate"]) && isset($_GET["enddate"])) {
    $startDate = $_GET["startdate"];
    $endDate = $_GET["enddate"];

  }


  if(!isset($_GET["startdate"])) {
    $startDate = date('Y-m-01');
  }

  if(!isset($_GET["enddate"])) {
    $endDate = date('Y-m-t');
  }


  function showNextMonthLink($startDate, $endDate) {
    $dayNextMonth = strtotime("+1 month", strtotime($startDate));

    $nextMonthStartDate = date('Y-m-01', $dayNextMonth);
    $nextMonthEndDate = date('Y-m-t', $dayNextMonth);

    echo "<a href=\"?startdate=$nextMonthStartDate&amp;enddate=$nextMonthEndDate\">Next Month</a>";
  }
  
  function showPreviousMonthLink($startDate, $endDate) {
    $dayPreviousMonth = strtotime("-1 month", strtotime($startDate));

    $previousMonthStartDate = date('Y-m-01', $dayPreviousMonth);
    $previousMonthEndDate = date('Y-m-t', $dayPreviousMonth);

    echo "<a href=\"?startdate=$previousMonthStartDate&amp;enddate=$previousMonthEndDate\">Previous Month</a>";
  }
  
  function showMonthnName($startDate) {
    echo date('F Y', strtotime($startDate));
  }


  function showCalendar($startDate, $endDate, $username) {


     $timesheetResponse = json_decode(scapi("GET", "timesheet?startdate=$startDate&enddate=$endDate", "", "localhost", $username), true);

        $timesheetDays = array();

        foreach($timesheetResponse as $day) {
          $timesheetDays[$day['workDate']] = $day;
        }


        //Mocks timesheet data between 2 dates
        $days=array();

          $iDateFrom=mktime(1,0,0,substr($startDate,5,2), substr($startDate,8,2),substr($startDate,0,4));
          $iDateFrom -= 86400;
          $iDateTo=mktime(1,0,0,substr($endDate,5,2), substr($endDate,8,2),substr($endDate,0,4));

          $today = date('Y-m-d');

        $firstDayDayofWeek = getDayOfWeek($startDate);

        for($i = 0; $i < $firstDayDayofWeek; $i += 1) {
            echo "<td>" . "X" ."</td>";          
        }


          if ($iDateTo>=$iDateFrom)
          {
              while ($iDateFrom<$iDateTo)
              {
                  $iDateFrom+=86400; // add 24 hours
                  $date = date('Y-m-d',$iDateFrom);

                  $currentDayOfTheWeek = getDayOfWeek($date);

                  //If the date is not in the future
                  if(strtotime($date)<strtotime($today)) {

                    echo "<td>";
                    echo "<a href=\"edittimesheet.php?date=$date\">";
                    echo $date;
                    echo "</a>";
                    echo "<br />";

                    if(!isset($timesheetDays[$date]) || sizeof($timesheetDays[$date]["workTimes"]) === 0) {
                      echo "<b>Update timesheet</b>";
                    } else {
                      foreach($timesheetDays[$date]["workTimes"] as $time) {
                        echo $time["workTime"] . " minutes on " . $time["jobId"] . "<br/>";
                      }
                    }
                  } else {
                    echo "<td>";
                    echo $date;
                    echo "<br />";
                  }

                  echo "</td>";

                  if($currentDayOfTheWeek === 6) {
                    echo "</tr><tr>";
                  }
              }
          }



            for($i = $currentDayOfTheWeek; $i < 6; $i += 1) {
            echo "<td>" . "X" ."</td>";          
        }
  }

?>



<ol class="breadcrumb">
  <li><strong>You are here: </strong></li>
  <li>Timesheets</li>
</ol>

</header>

<div class="container-fluid">
  <div class="row">
    <?php showSaved() ?> 


    <h2><?php showMonthnName($startDate) ?></h2>
    
    <?php showPreviousMonthLink($startDate, $endDate); ?>
    <?php showNextMonthLink($startDate, $endDate); ?>


<div class="table-responsive">
    <table class="table-condensed table-bordered table-striped">
      <tr>
        <th>
          Mon
        </th>
        <th>
          Tue
        </th>
        <th>
          Wed
        </th>
        <th>
          Thu
        </th>
        <th>
          Fri
        </th>
        <th>
          Sat
        </th>
        <th>
          Sun
        </th>
      </tr>
      <tr>
      <?php showCalendar($startDate, $endDate, $username) ?>
    </tr>
    </table>
</div>
  </div>
</div>


<?php include 'partials/footer.php' ?>
