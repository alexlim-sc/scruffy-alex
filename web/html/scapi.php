<?php

require('../../creds.php');

function scapi($httpVerb, $resource, $data, $host="localhost", $maskuser="", $version="v0") {

	global $creds;

	if(isset($creds[$host])) {
		$username = $creds[$host]['username'] ;

		if($maskuser !== "") {
			$username = $username."/".$maskuser;
		}

		$password = $creds[$host]['password'];
		$scapiurl = $creds[$host]['scapiurl'];
	} else {
		return "{\"error\":\"You must set the creds for " . $host . "\"}";
	}

	$nonce = "1234abcd4321";
	$algorithmTag = "SCAPI0-HMAC-SHA256";
	$datetime = new DateTime(null, new DateTimeZone("UTC"));
	//$datetime->add(new DateInterval('PT' . 10 . 'M'));
	$timestamp = $datetime->format('c');
	$timestamp = substr($timestamp,0,19) . "Z";

	$endpoint = $scapiurl.$version."/".$resource;

	$preHash = $algorithmTag . "+" . $username . "+" . $httpVerb . "+" . $endpoint . "+" . $timestamp . "+" . $nonce . "+" . $data;
	$hash = hash_hmac('sha256', $preHash, $password);
	$base64 = base64_encode($hash);


	$authHeader = $algorithmTag . ":" . $username . ":" . $nonce . ":" . $base64;
	$xScapiDate = $timestamp;


	// Get cURL resource
	$curl = curl_init();
	// Set some options - we are passing in a useragent too here
	curl_setopt_array($curl, array(
	 	CURLOPT_HTTPHEADER  => array('X-SCAPI-Date:'.$xScapiDate, 'Authentication: '.$authHeader,'Content-Type: application/json'),
	    CURLOPT_RETURNTRANSFER => 1,
	    CURLOPT_URL => $host.$endpoint,
	    CURLOPT_POSTFIELDS => $data,
	    CURLOPT_USERAGENT => 'Codular Sample cURL Request',
	    CURLOPT_CUSTOMREQUEST => $httpVerb
	));
	// Send the request & save response to $resp
	$resp = curl_exec($curl);
	// Close request to clear up some resources
	curl_close($curl);

	return $resp;
}