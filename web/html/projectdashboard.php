<?php

  require("scapi.php");
  

  $pageTitle = "Sceptre";
  include 'partials/header.php';



  $projectStatusList = json_decode(scapi("GET", "projectstatus",""), true);

  $projectStatuses = array();
  foreach($projectStatusList as $status) {
    $x = json_decode(scapi("GET", "projectstatus/".$status['id'],""), true);
    $projectStatuses[$status['id']] = $x['name'];
  }


echo $projectStatuses["0"];

  
   function getDelay($planned, $estimated, $actual) {
    if(!isset($planned)) {
        return 0;
    } else {
        if(isset($actual)) {
            return dateDiff($planned, $actual);
        } else if(isset($estimated)) {
            return dateDiff($estimated, $actual);
        } else {
            return 0;
        }

    }

   }

   function plural($number, $noun) {
    if($number != 1) {
        return $number . " " . $noun."s";
    } else {
        return $number . " " . $noun;
    }
   }

   function siPrefix($number, $unit, $decimals) {
    if(!isset($decimals)) {
        $decimals = 2;
    }

    if($number > 1000000) {
        return number_format((float)($number/1000000), 2, '.', '') . " M" . $unit;
    } else if ($number > 1000) {
        return number_format((float)($number/1000), 2, '.', '') . " k" . $unit;
    } else {
        return number_format((float)($number), 2, '.', '') . " " . $unit;
    }
   }

   function dateDiff($firstDate, $secondDate) {
     $time1 = strtotime($firstDate); // or your date as well
     $time2 = strtotime($secondDate);
     $datediff = $time2 - $time1;
     return floor($datediff/(60*60*24));
 }

  function showProjects() {
    global $projectStatuses;


    $projects = json_decode(scapi("GET", "projectdashboard",""), true);

      echo "<thead>";
      echo "<tr>";
        echo "<th>";
        echo "Client";
        echo "</th>";
        echo "<th>";
        echo "Job ID";
        echo "</th>";
        echo "<th>";
        echo "Project name";
        echo "</th>";
        echo "<th>";
        echo "Size";
        echo "</th>";
        echo "<th>";
        echo "Status";
        echo "</th>";
        echo "<th>";
        echo "Connection planned";
        echo "</th>";
        echo "<th>";
        echo "Connection estimated";
        echo "</th>";
        echo "<th>";
        echo "Connection actual";
        echo "</th>";
        echo "<th>";
        echo "Connection Delay";
        echo "</th>";
        echo "<th>";
        echo "PAC planned";
        echo "</th>";
        echo "<th>";
        echo "PAC estimated";
        echo "</th>";
        echo "<th>";
        echo "PAC actual";
        echo "</th>";
        echo "<th>";
        echo "PAC Delay";
        echo "</th>";
        echo "<th>";
        echo "Progress";
        echo "</th>";

      echo "</tr>";
      echo "</thead>";

echo "<tbody>";
    foreach($projects as $project) {

      



      echo "<tr>";
        echo "<td>";
        echo $project['clientName'];
        echo "</td>";
        echo "<td>";
        echo $project['jobId'];
        echo "</td>";
        echo "<td>";
        echo $project['projectName'];
        echo "</td>";
        echo "<td>";
        echo siPrefix($project['size'], "W");
        echo "</td>";
        echo "<td>";
        echo $projectStatuses[$project['projectStatus']];
        echo "</td>";
        echo "<td>";
        echo $project['connectionPlannedDate'];
        echo "</td>";
        echo "<td>";
        echo $project['connectionEstimatedDate'];
        echo "</td>";
        echo "<td>";
        echo $project['connectionActualDate'];
        echo "</td>";
        echo "<td>";
        $delay = getDelay($project['connectionPlannedDate'], $project['connectionEstimatedDate'], $project['connectionActualDate']);
        echo plural($delay,"day");
        echo "</td>";
        echo "<td>";
        echo $project['pacPlannedDate'];
        echo "</td>";
        echo "<td>";
        echo $project['pacEstimatedDate'];
        echo "</td>";
        echo "<td>";
        echo $project['pacActualDate'];
        echo "</td>";
        echo "<td>";
        $delay = getDelay($project['pacPlannedDate'], $project['pacEstimatedDate'], $project['pacActualDate']);
        echo plural($delay,"day");
        echo "</td>";
        echo "<td>";
        echo sprintf("%.2f%%", $project['progress'] * 100);
        echo "</td>";

      echo "</tr>";
    }
echo "</tbody>";
  }

?>



<ol class="breadcrumb">
  <li><strong>You are here: </strong></li>
  <li>Project Dashboard</li>
</ol>

</header>

<div class="container-fluid">
  <div class="row">


    <div class="table-responsive">

        <table id="projectDashboardTable" class="table-condensed table-bordered table-striped">
          
          <?php showProjects($projects); ?>
        
        </table>
    </div>

  </div>
</div>

<?php include 'partials/footer.php' ?>
