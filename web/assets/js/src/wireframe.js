// datepicker
function initDatepicker() {
    $('.datepicker').datepicker({
        dateFormat: "yy-mm-dd"
    })
}

rowTemplate = ""



function getTimerowTemplate() {
    
    return $.get("timerowtemplate.html");
}

function getMessageTemplate() {
    
    return $.get("messagetemplate.html");
}

function getJobs() {
    var deferred = $.Deferred();
    $.get("jobs.php", function( data ) {
      
        var i;
        data = JSON.parse(data);
        var jobs = [];
        for(i = 0; i < data.length; i += 1) {
            jobs.push(data[i].name)
        }
        deferred.resolve(jobs);
    });
    return deferred.promise();

}


var substringMatcher = function(strs) {
          return function findMatches(q, cb) {
            var matches, substringRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function(i, str) {
              if (substrRegex.test(str)) {
                matches.push(str);
              }
            });

            cb(matches);
          };
        };

(function ($) {

    $(document).ready(function () {



    if($('#projectDashboardTable').DataTable) {
        $('#projectDashboardTable').DataTable({ responsive: true});
    }


        $.when(getTimerowTemplate(), getMessageTemplate(), getJobs()).done(function(templateResult, messageTemplateResult, jobs) {

            enableTimeRow($("#timerows").find(".timerow"));


            $("#timeform").on('submit',function(e) {

                var message = "";

                $(".messageBox").remove();

                $("input.jobname.tt-input").each(function() {
                    if($(this).val() === "") {
                        message += "Job name can not be blank </br>";
                    }
                });

                $(".jobcomment").each(function() {
                    if($(this).val() === "") {
                        message += "Comment can not be blank </br>";
                    }
                });

                $(".jobtime").each(function() {
                    if($(this).val() == "") {
                        message += "Time can not be blank </br>";
                    } 
                });

                if(message === "") {
                    
                    
                } else {
                    
                    var messageTemplate = messageTemplateResult[0];
                    var messagebox = $(messageTemplate);
                    messagebox.find(".title").html("Error");
                    messagebox.find(".message").html(message);

                    $("h1").before(messagebox);
                    e.preventDefault();
                    
                    
                }

                

                
            });

            var template = templateResult[0];
            function addTimeRow() {
                var timerow = $(template);
                var rowCount = parseInt($("#rowCount").val(), 10);
                rowCount += 1;
                $("#rowCount").val(rowCount)

                timerow.find(".jobname").attr("name", "rows["+ rowCount +"][name]");
                timerow.find(".jobcomment").attr("name", "rows["+ rowCount +"][comment]");
                timerow.find(".jobtime").attr("name", "rows["+ rowCount +"][workTime]");


                $("#timerows").append(timerow);
                
                enableTimeRow(timerow);
            }

        
            function enableTimeRow(timerow) {
                timerow.find(".typeahead").typeahead({
                  hint: true,
                  highlight: true,
                  minLength: 1
                },
                {
                  name: 'Jobs',
                  source: substringMatcher(jobs)
                });

                timerow.find(".remove").click(function() {
                    $(this).parents(".timerow").remove();
                })
            }     

            $("#addTimeRow").click(function() {
                addTimeRow();
            });

            
        }); 
        
  
        
        


        // init tooltips for all data-tooltips
        $('.data-tooltip').tooltip({
            animation: false
        });

        function expandCollapseAll (action, collapseItems) {

            if( action == "expand") {
                $(collapseItems).each( function() {
                    //console.log($(this).attr('class'));
                    if($(this).hasClass('collapsed')) {
                        $(this).click();
                    }
                });
            }

            if( action == "collapse") {
                $(collapseItems).each( function() {
                    if(!$(this).hasClass('collapsed')) {
                        $(this).click();
                    }
                });
            }
        }

    });
})(jQuery);
