function sortBy() {
  var x = document.getElementById("edit-sort-by").value;
  url = window.location.href;
  if(url.indexOf("?") != -1) {
    var url_split = url.split("?");
    var position = "";
    var new_value = "";
    var new_url = "";
    if(url.indexOf("&") != -1) {
      var query_string = url_split[1].split("&");
      position = String(query_string).search('sort_by=');
    } else {
      position = url.search('sort_by=');
    }
    console.log(position);
    if(position != -1) {
      var value = query_string[position].split('=');
      new_value = url_split[1].replace(String(value[1]), x);
    } else {
      new_value = url_split[1] + "&sort_by=" + x;
    }
    new_url = url_split[0] + "?" + new_value;
  }else{
    new_url = url + "?sort_by=" + x;
  }
  window.location.href = new_url;
}

(function ($) {

  $(document).ready(function () {
    // init tooltips for all data-tooltips
    $('.data-tooltip').tooltip({
        animation: false
    });

    // Search form.
    $('#search-collapse').submit(function () {
      var term = $('#search-collapse input[type=text]').val();
      $(this).attr('action', '/search/' + term);
    });

    function expandCollapseAll (action, collapseItems) {

      if( action == "expand") {
        $(collapseItems).each( function() {
          console.log($(this).attr('class'));
          if($(this).hasClass('collapsed')) {
            $(this).click();
          }
        });
      }

      if( action == "collapse") {
        $(collapseItems).each( function() {
          if(!$(this).hasClass('collapsed')) {
            $(this).click();
          }
        });
      }
    }

    // expand / collapse all tasks
    $('.tickets-controls .expand-all-trigger').click( function(e) {
      expandCollapseAll ("expand", 'a.collapse-link');
      e.preventDefault();
    });
    $('.tickets-controls .close-all-trigger').click( function(e) {
      expandCollapseAll ("collapse", 'a.collapse-link');
      e.preventDefault();
    });

    // expand /collapse all docs
    $('.documents-controls .expand-all-trigger').click( function(e) {
      expandCollapseAll ("expand", '.documents-tree .folder');
      e.preventDefault();
    });
    $('.documents-controls .collapse-all-trigger').click( function(e) {
      expandCollapseAll ("collapse", '.documents-tree .folder');
      e.preventDefault();
    });

    // quick find document selector
    $('.quickfind-document').change( function() {
      // get the value selected in the dropdown
      selectedValue = $(this).find('option:selected').val();

      // collapse the document tree
      $(".documents-controls .collapse-all-trigger").click();

      // find the arrow by searching for the rel
      arrow = $("[rel=" + selectedValue + "]");
      x = arrow;

      // while you've not reached the top of the folder tree
      do {
        // find the section the object is in
        section = x.closest('section');

        // find the folder that opens this section
        folder = section.prev('.folder');

        // check the folder is collapsed, if it is open it
        if (folder.hasClass('collapsed')) {
          folder.click();
        }

        // make the folder our lopping object
        x = folder;
      }
        // check if the object has a parent section
      while (x.closest('section').length > 0);

      // scroll down to the arrow
      $('html, body').animate({scrollTop: arrow.closest('.file, .folder').offset().top}, 1200);
    });

    // missing arrows popover
    $('#missing-arrows .popover-trigger').popover({
      trigger: 'focus',
      html : true,
      placement: 'bottom',
      content: function() {
        return $('#missing-arrows .content').html();
      }
    });

    // iteration descriptions
    $('.iterations-list span.desc').popover({
      trigger: 'hover',
      placement: 'top',
      content: function() {
        return $(this).attr('data-content');
      }
    });

    // fix arrow position
    $('.iterations-list span.desc').on('shown.bs.popover', function () {
      $('.popover .arrow').css('top',parseInt($('.popover .arrow').css('top', '100%')));
    });

    // version popovers
    // data-container=".version-table" data-placement="left" data-trigger="hover" data-toggle="popover" data-html="true"

    $('.documents-tree .folder .version-table td').each( function() {
      console.log ($(this).attr('data-content'));

      $(this).popover({
        animation: 'false',
        trigger: 'hover',
        html : true,
        placement: 'left',
        content: function() {
          return $(this).attr('data-content');
        },
        container: $(this).closest('.name')
      });
    });

    // todo: this can probably use a popover event to make it cleaner
    $('#missing-arrows .popover-trigger').click( function(e) {
      e.preventDefault();
    });

    $('#addArrowModal').on('show.bs.modal', function (e) {
      console.log(e);
      e.stopPropagation();
    });

    // handle clicks on a folder and arrow placement
    $('.documents-tree .folder').click(function(e) {
      if ($(e.target).parent().attr('class') == 'place-arrow') {
        $('#addArrowModal').modal('show');
      }
      else if ($(e.target).parent().attr('class') == 'edit-folder') {
        $('#addEditFolder').modal('show');
      }
      else {
        $(this).toggleClass("collapsed").next("section").slideToggle();
      }
    });

    // handle clicks on a file and arrow placement
    $('.documents-tree .file a').click(function(e) {
      if ($(e.target).parent().attr('class') == 'place-arrow') {
        $('#addArrowModal').modal('show');
        e.preventDefault();
      }
    });

    // show message on change if selcted is already assigned
    $('select.assign-arrow').on( "change", function() {
      if ( typeof $(this).find('option:selected').attr('rel') === "undefined") {
        $('.arrow-overwrite-notice').html('');
      }
      else {
        $('.arrow-overwrite-notice').html('<hr/><p><span class="glyphicon glyphicon-warning-sign"></span> Selecting this arrow will remove it from: <strong>' + $(this).find('option:selected').attr('rel') + '</strong></p>');
      }
    });


    $('.versioning .version-table .version-progress-bar').each( function() {
      complete = $(this).attr('rel');
      remaining = 100 - complete;

      $(this).css('height', remaining + "%");
    });
  });
})(jQuery);
